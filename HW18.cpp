﻿# include <iostream>

class Stack
{
private:
	int y;
public:
	void cin(int *&a, int &n)   //Запись в массив
	{
		std::cout << "Enter the numbers: " << '\n';
		for (int i = 0; i < n; i++)
		{
			std::cin >> y;
			a[i] = y;     
		}
		std::cout << '\n';
	}

	void mas(int a[], int &n)   //Вывод элементов массива
	{
		std::cout <<'\n' << "Steck: " << '\n';
		for (int i = 0; i < n; i++)
		{
			std::cout << a[i] << '\n';
		}
	}

	void re(int *&a, int &n)   //Действие над стеком
	{
		int v;

		std::cout << '\n' << "Press '0' - delete element, Press '1' - add element, Press '2' - exit" << '\n';
		std::cin >> v;
	
		switch(v)
		{
		case 0:
			std::cout << '\n' << "Result pop";
			pop(a, n);
			mas(a, n);
			re(a, n);
			break;

		case 2:
			std::cout << '\n' << "Press any key";
			break;

		case 1:
			push(a, n);
			std::cout << '\n' << "Result push";
			mas(a, n);
			re(a, n);
			break;
		}
	}

	void pop(int *&a, int &n)   //Удаление последнего элемента
	{
		n--;
		int* newA = new int[n];
		for (int i = 0; i < n; i++)
		{
			newA[i] = a[i];
		}
		delete[] a;
		a = newA;
	}
	void push(int*& a, int& n)   //Добавление нового элемента
	{
		int v = 0;
		n++;

		int* newA = new int[n];

		std::cout << '\n' << "Enter a new number: " << '\n';
		std::cin >> v;
		for (int i = 0; i < n; i++)
		{
			newA[i] = a[i];
		}
		newA[n-1] = v;
		delete[] a;
		a = newA;
	}
};

int main()
{
	int n;

	std::cout << "Specify the Stack size: " << '\n';
	std::cin >> n;
	std::cout << '\n';
	int* a = new int[n]; // выделили место из памяти

	Stack write;
	write.cin(a, n);
	write.mas(a, n);
	write.re(a, n);
}